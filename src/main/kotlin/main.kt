fun main(args: Array<String>) {

    /*
    val message = "Hello Javascript!"
    println(message)
    */
    /*
      TODO       "Create an App using Javascript that, when run, presents a deck's worth of cards in random order"

        TODO       + "The Ace of Spades is now Card.id = 1 and all Aces are Card.worth = 1"

          TODO        + "First: Create new Gradle Javascript project, Second: Upload project to Gitlab!!!"

    */
}

data class Card(val id: Int, var worth: Int?)

class Deck {
    var deck = mutableListOf<Card>()
    fun newDeck() {
        deck.clear()
        // Gives each card an id from 1 to 52
        for (x in 1..52) {
            deck.add(Card(x, null))
        }

        for (y in 0..3) {
            //Give face cards a worth of 10
            for (z in 1..3) {
                deck[z - 1 + (13 * y)].worth = 10
            }
            //Give cards Ace to Ten a worth of 1 to 10
            for (w in 1..10) {
                deck[w - 1 + (13 * y)].worth = w
            }
        }

        deck.shuffle()
    }
    fun cardName(card: Card): String {
        val numbers = listOf( "Ace" + "Two" + "Three" + "Four" + "Five" + "Six" + "Seven" + "Eight" + "Nine" + "Ten" + "Jack" + "Queen" + "King")
        val suits = listOf("Spades" + "Clubs" + "Hearts" + "Diamonds")
        //Builds and returns a String according to the Card's id

        return when {
            //The Card is a Spade
            card.id <= 13 -> suits[0] + numbers[card.id - 1]
            //The Card is a Club
            card.id <= 26 -> suits[1] + numbers[(card.id - 1) - 13]
            //The Card is a Heart
            card.id <= 39 -> suits[2] + numbers[(card.id - 1) - 26]
            //The Card is a Diamond
            card.id <= 52 -> suits[3] + numbers[(card.id - 1) - 39]

            else -> throw Exception("Error, Deck.cardName() has a bug or a Card with a null id was passed")
        }
    }
}

/*
class Deck {
    var deck = mutableListOf<Card>()
    fun newDeck() {
        deck.clear()
        for (i in 0..3)
            for (x in 2..14) {
                deck.add(Card((x - 1) + (i * 10), x)
                )
                for (y in 11..13) {
                    deck[y + (i * 10)].worth = 10
                }
            }
    }
}
 */
